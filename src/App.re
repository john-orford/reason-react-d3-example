let component = ReasonReact.statelessComponent("App");

let make = _children => {
  /* spread the other default fields of component here and override a few */
  ...component,
  render: _self =>
    <div className="container is-fluid">
      <div className="title level"> (ReasonReact.string("Input Data")) </div>
      <FetchP />
    </div>,
};
