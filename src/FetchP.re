/*
 Based on:

 https://github.com/reasonml-community/reason-react-example/blob/master/src/fetch/FetchExample.re
 */

open Belt;

type state =
  | Loading
  | Error
  | Loaded(Types.points);

type action =
  | Fetch
  | Fetched(Types.points)
  | FailedToFetch;

module Decode = {
  let point = json : Types.point =>
    Json.Decode.{
      x: json |> field("x", string) |> DateFns.parseString,
      y: json |> field("y", int),
    };

  let points = json : Types.points =>
    Json.Decode.(
      json |> field("values", array(point)) |> Array.map(_, point => point)
    );
};

let component = ReasonReact.reducerComponent("FetchP");

let make = _child => {
  ...component,
  initialState: _state => Loading,
  reducer: (action, _state) =>
    switch (action) {
    | Fetch =>
      ReasonReact.UpdateWithSideEffects(
        Loading,
        (
          self =>
            Js.Promise.(
              Fetch.fetch("http://konuxdata.getsandbox.com/data")
              |> then_(Fetch.Response.json)
              |> then_(json =>
                   json
                   |> Decode.points
                   |> (points => self.send(Fetched(points)))
                   |> resolve
                 )
              |> catch(err => {
                   Js.log(err);
                   Js.Promise.resolve(self.send(FailedToFetch));
                 })
              |> ignore
            )
        ),
      )
    | Fetched(ds) => ReasonReact.Update(Loaded(ds))
    | FailedToFetch => ReasonReact.Update(Error)
    },
  didMount: self => self.send(Fetch),
  render: self =>
    switch (self.state) {
    | Error => <div> (ReasonReact.string("An error occurred!")) </div>
    | Loading => <div> (ReasonReact.string("Loading...")) </div>
    | Loaded(points) =>
      /*
       Make polymorphic: https://jaredforsyth.com/posts/advanced-reasonreact-hider-order-components/
       */
      <Send fetchedPoints=points />
    },
};
