
/* FFI to D3 js code */
[@bs.module "./d3LineChart"]
external chart : (Types.points, Dom.element) => unit = "default";

type state = {points: Types.points, ref: ref(option(Dom.element))};

/* dummy but required */
type action = A;

/* get points and ref, and create graph */
let setRef = (theRef,{ReasonReact.state} ) => {
  let maybeNode = Js.Nullable.toOption(theRef);
 
  switch maybeNode {
  | Some(node) => chart(state.points, node);
  /* do nothing */
  | _ => ()
  };

  /* why necessarily mutable? */
  state.ref := maybeNode;
};

let component = ReasonReact.reducerComponent("Graph");

let make = (~points: Types.points, _children) => {
  /* spread the other default fields of component here and override a few */
  ...component,
  initialState: _s => {points: points, ref: ref(None)},
  /* reducer is a dummy, not used but required */
  /* action is also a dummy, but required*/
  reducer: (action: action, state: state) => ReasonReact.Update(state),
  willReceiveProps: self => {...self.state, points: points},
  render: ({handle}) =>
    <div>
      <div ref=handle(setRef) />      
    </div>,
};
