/*
 Loosely based on:

 https://github.com/reasonml-community/reason-react-example/blob/master/src/fetch/FetchExample.re
 */

open Belt;

/* possible component status */
type status =
  | OK
  | Sending
  | Error
  | Ready;

/* state, more validation required and more restrictive typing, to ensure only valid data is ever sent etc. */
type state = {
  status,
  point: Types.point,
  addedPoints: Types.points,
};

/* possible form actions */
type action =
  | Send
  | Sent
  | FailedToSend
  | YChange(int)
  | XChange(Js.Date.t);

/* encode data to be sent */
module Encode = {
  let point = (p: Types.point) =>
    Json.Encode.(
      object_([
        ("x", p.x |> Js.Date.toISOString |> string),
        ("y", int(p.y)),
      ])
    );
};

let component = ReasonReact.reducerComponent("Send");

/* points from fetch component */
let make = (~fetchedPoints: Types.points, _child) => {
  ...component,
  initialState: _state => {
    status: Ready,
    point: {
      x: Js.Date.now() |> DateFns.parseFloat,
      y: 0,
    },
    addedPoints: [||],
  },
  reducer: (action, state) =>
    switch (action) {
    | Send =>
      ReasonReact.UpdateWithSideEffects(
        {...state, status: Sending},
        (
          self =>
            Js.Promise.(
              Fetch.fetchWithInit(
                "http://konuxdata.getsandbox.com/points",
                Fetch.RequestInit.make(
                  ~method_=Post,
                  ~body=
                    Fetch.BodyInit.make(
                      Encode.point(state.point) |> Json.stringify,
                    ),
                  ~headers=
                    Fetch.HeadersInit.make({
                      "Content-Type": "application/json",
                    }),
                  ~mode=NoCORS,
                  (),
                ),
              )
              |> then_(_ => self.send(Sent) |> resolve)
              |> catch(err => {
                   Js.log(err);
                   resolve(self.send(FailedToSend));
                 })
              |> ignore
            )
        ),
      )
    | Sent =>
      ReasonReact.Update({
        ...state,
        status: OK,
        addedPoints: Array.concat(state.addedPoints, [|state.point|]),
      })
    | FailedToSend => ReasonReact.Update({...state, status: Error})
    | YChange(y) =>
      ReasonReact.Update({
        ...state,
        point: {
          ...state.point,
          y,
        },
      })
    | XChange(x) =>
      ReasonReact.Update({
        ...state,
        point: {
          ...state.point,
          x,
        },
      })
    },
  render: ({state, send}) =>
    <div className="">
      <div className="level">
        <div className="level-left">
          /* on change, update state */

            <input
              className="input is-primary level-item"
              type_="datetime-local"
              onChange=(
                event =>
                  send(
                    XChange(
                      ReactDOMRe.domElementToObj(
                        ReactEventRe.Form.target(event),
                      )##value
                      |> DateFns.parseString,
                    ),
                  )
              )
            />
            <input
              type_="number"
              className="input is-primary level-item"
              onChange=(
                event =>
                  send(
                    YChange(
                      ReactDOMRe.domElementToObj(
                        ReactEventRe.Form.target(event),
                      )##value,
                    ),
                  )
              )
            />
            <button
              className="button is-primary level-item"
              onClick=(
                event => {
                  ReactEventRe.Synthetic.preventDefault(event);
                  send(Send);
                }
              )>
              (ReasonReact.string("Add"))
            </button>
            
             <div className="level-right">
               <div className="level-item">
              (
                switch (state.status) {
                | Ready => <div> (ReasonReact.string("Ready")) </div>
                | Error =>
                  <div> (ReasonReact.string("An error occurred!")) </div>
                | Sending => <div> (ReasonReact.string("Sending...")) </div>
                | OK => <div> (ReasonReact.string("OK")) </div>
                }
              )
             </div>
             </div>
           </div>

      </div>

           <div className="level">
             <div className="level-left">
               <div className="level-item">
                  /* should make polymorphic, switch in and out display components */
                 <Graph points=(Array.concat(fetchedPoints, state.addedPoints)) />
               </div>
             </div>
                   </div>
    </div>,
};
