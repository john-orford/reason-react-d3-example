
/* Globally used types - much more can be done to ensure validity */

type point = {
  x: Js.Date.t,
  y: int,
};
type points = array(point);
