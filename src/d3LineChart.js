// based on: http://bl.ocks.org/weiglemc/6185069

// There is a nascent Reason ML bindings library, which might be worth looking at in future

import * as d3 from "d3";

const f = (points, node) =>
      {
	  const data = points
		.sort((p1,p2) => p1[0] > p2[0])
		.map(d => [+d[0], d[1]]);

	  const margin = {top: 20, right: 20, bottom: 30, left: 40},
		width = 960 - margin.left - margin.right,
		height = 500 - margin.top - margin.bottom;

	  /* 
	   * value accessor - returns the value to encode for a given data object.
	   * scale - maps value to a visual display encoding, such as a pixel position.
	   * map function - maps from data value to display value
	   * axis - sets up axis
	   */ 

	  // setup x 
	  const xValue = d => d[0], // data -> value
		xScale = d3.scaleTime().range([0, width]), // value -> display
		xMap = d => xScale(xValue(d)), // data -> display
		xAxis = d3.axisBottom(xScale);

	  // setup y
	  const yValue = d => d[1], // data -> value
		yScale = d3.scaleLinear().range([height, 0]), // value -> display
		yMap = d => yScale(yValue(d)), // data -> display
		yAxis = d3.axisLeft(yScale);

	  
	  const line = d3.line()
	      .x( d => xScale(d[0]) ) // set the x values for the line generator
	      .y(d => yScale(d[1]) ) // set the y values for the line generator 
	      .curve(d3.curveMonotoneX) // apply smoothing to the line
	  
	  // setup fill color
	  const color = d3.scaleOrdinal(d3.schemeCategory10);

	  // updates should be handled more on React side
	  d3.selectAll("svg").remove();
	  
	  // add the graph canvas to the body of the webpage
	  const svg = d3.select(node).append("svg")
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
		.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	  // add the tooltip area to the webpage
	  const tooltip = d3.select(node).append("div")
		.attr("class", "tooltip")
		.style("opacity", 0);

	  // don't want dots overlapping axis, so add in buffer to data domain
	  xScale.domain([d3.min(data, xValue)-1, d3.max(data, xValue)+1]);
	  yScale.domain([d3.min(data, yValue)-1, d3.max(data, yValue)+1]);

	  // x-axis
	  svg.append("g")
	      .attr("class", "x axis")
	      .attr("transform", "translate(0," + height + ")")
	      .call(xAxis)
	      .style("fill", "black") 
	      .append("text")
	      .attr("class", "label")
	      .attr("x", width)
	      .attr("y", -6)
	      .style("text-anchor", "end")
	      .text("Time");

	  // y-axis
	  svg.append("g")
	      .attr("class", "y axis")
	      .call(yAxis)
	      .style("fill", "black") 
	      .append("text")
	      .attr("class", "label")
	      .attr("transform", "rotate(-90)")
	      .attr("y", 6)
	      .attr("dy", ".71em")
	      .style("text-anchor", "end")
	      .text("Value");

	  svg.append("path")
              .attr("stroke", "black")
              .attr("fill", "none")
	      .datum(data) // 10. Binds data to the line 
	      .attr("class", "line") // Assign a class for styling 
	      .attr("d", line); // 11. Calls the line generator
	      
	  
	  // draw dots
	  svg.selectAll(".dot")
	      .data(data)
	      .enter().append("circle")
	      .attr("class", "dot")
	      .attr("r", 3.5)
	      .attr("cx", xMap)
	      .attr("cy", yMap)
	      .style("fill", _d => color(null)) 
	      .on("mouseover", d => {
	  	  tooltip.transition()
	  	      .duration(200)
	  	      .style("opacity", 1);
	  	  tooltip.html( "Time: " + (new Date(xValue(d)))
	  			+ "<br /> Value: " + yValue(d) )
	  	      .style("left", (d3.event.pageX + 5) + "px")
	  	      .style("top", (d3.event.pageY - 28) + "px");
	      })
	      .on("mouseout", d => 
	  	  tooltip.transition()
	  	      .duration(500)
	  	      .style("opacity", 0)
	      );

	  // draw legend
	  const legend = svg.selectAll(".legend")
	  	.data(color.domain())
	  	.enter().append("g")
	  	.attr("class", "legend")
	  	.attr("transform", (d, i) => "translate(0," + i * 20 + ")");

	  // draw legend colored rectangles
	  legend.append("rect")
	      .attr("x", width - 18)
	      .attr("width", 18)
	      .attr("height", 18)
	      .style("fill", color);

	  // draw legend text
	  legend.append("text")
	      .attr("x", width - 24)
	      .attr("y", 9)
	      .attr("dy", ".35em")
	      .style("text-anchor", "end")
	      .text("Data Points")
	  // });


      };



export default f;


